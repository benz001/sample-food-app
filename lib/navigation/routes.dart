import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:sample_food_app/provider/food_provider.dart';
import 'package:sample_food_app/view/detail_product_screen.dart';
import 'package:sample_food_app/view/home_screen.dart';

// sebagai kumpulan navigasi dari aplikasi

class Routes extends StatelessWidget {
  const Routes({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<FoodProvider>(
      create: (context) => FoodProvider(),
      child: MaterialApp(
        title: 'Food App',
        initialRoute: '/homeScreen',
        debugShowCheckedModeBanner: false,
        theme: new ThemeData(
          primarySwatch: Colors.teal,
          canvasColor: Colors.transparent,
        ),
        routes: {
          HomeScreen.routeName: (context) => HomeScreen(),
          DetailProductScreen.routeName: (context) => DetailProductScreen()
          },
      ),
    );
  }
}
