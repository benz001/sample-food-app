// Bagian dari layer model untuk menggambarkan struktur model dari ap
class Food {
  int id;
  String name;
  String cover;
  String desc;
  int price;

  Food({this.id, this.name, this.cover, this.desc, this.price});

  factory Food.fromJson(Map<String, dynamic> json){
    return Food(
      id: json['id'],
      name: json['name'],
      cover: json['cover'],
      desc: json['desc'],
      price: json['price']
    );
  }

  @override
  String toString() {
    return 'Food{id: $id, name: $name, desc: $desc, price: $price}';
  } 
}