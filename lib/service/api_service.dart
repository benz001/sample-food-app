import 'dart:convert';

import 'package:sample_food_app/model/food.dart';
import 'package:http/http.dart' as http;

// Bagian dari layer controller untuk menghubungkan antara model dan view

class APIService {

  // Fungsi untuk mengambil data dari api
  List<Food> foodFromJson(String jsonData) {
    List data = json.decode(jsonData);
    return List<Food>.from(data.map((item) => Food.fromJson(item)));
  }

  Future<List<Food>> getListFood() async {
    final response = await http
        .get('https://mocki.io/v1/52c41978-6e31-4ea3-b917-01899e3ed373');
    try {
      if (response.statusCode == 200) {
        return foodFromJson(response.body);
      } else {
        return [];
      }
    } catch (e) {
      return [];
    }
  }
}
