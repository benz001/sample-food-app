import 'package:flutter/cupertino.dart';
import 'package:sample_food_app/model/food.dart';

// Bagian dari layer provider untuk menyimpan state global dan menshare nya ke layer view


class FoodProvider with ChangeNotifier {
  //List of note
  Food _food = Food(id: 0, name: '', cover: '', desc: '', price: 0);

  Food get getFood {
    return _food;
  }

  // FoodProvider() {
  //   addNewFood(0, 'name', 'cover', 'desc', 0);
  // }

  void addNewFood(int id, String name, String cover, String desc, int price) {
    //Note object
    Food food = Food(id: id, name: name, cover: cover, desc: desc, price: price);
    _food = food;
    // _food.add(food);
    notifyListeners();
  }
}