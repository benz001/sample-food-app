import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:sample_food_app/model/food.dart';
import 'package:sample_food_app/provider/food_provider.dart';
import 'package:sample_food_app/view/detail_product_screen.dart';
import 'package:sample_food_app/service/api_service.dart';

// Bagian dari layer view untuk menampilkan ui ke user
class HomeScreen extends StatefulWidget {
  static const routeName = '/homeScreen';
  const HomeScreen({Key key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  var apiService = APIService();
  @override
  void initState() {
    super.initState();
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
    apiService.getListFood().then((value) => print(value));
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    // final double itemHeight = (height - kToolbarHeight - 24) / 2;
    final double itemWidth = width / 2;

    Widget _itemFood(Food item) {
      return Container(
        child: GestureDetector(
          onTap: () {
            Provider.of<FoodProvider>(context, listen: false).addNewFood(
                item.id, item.name, item.cover, item.desc, item.price);
            Navigator.pushNamed(context, DetailProductScreen.routeName);
          },
          child: Card(
            child: Column(
              children: <Widget>[
                Expanded(
                    flex: 6,
                    child: GestureDetector(
                      onTap: () {
                        Provider.of<FoodProvider>(context, listen: false)
                            .addNewFood(item.id, item.name, item.cover,
                                item.desc, item.price);
                        Navigator.pushNamed(
                            context, DetailProductScreen.routeName);
                      },
                      child: Container(
                        decoration: BoxDecoration(
                            color: Colors.grey[300],
                            image: DecorationImage(
                                fit: BoxFit.fill,
                                image: NetworkImage(item.cover))),
                      ),
                    )),
                Expanded(
                    flex: 4,
                    child: GestureDetector(
                      onTap: () {
                        Navigator.pushNamed(
                            context, DetailProductScreen.routeName);
                      },
                      child: Container(
                        alignment: Alignment.topLeft,
                        // color: Colors.red,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              item.name,
                              style: TextStyle(fontSize: 16),
                            ),
                            Text(
                              'Rp. ' + item.price.toString(),
                              style: TextStyle(
                                  fontWeight: FontWeight.bold, fontSize: 16),
                            )
                          ],
                        ),
                      ),
                    )),
              ],
            ),
          ),
        ),
      );
    }

    Widget _itemMenuAlt(Color color, IconData icon, String menuName) {
      return Padding(
          padding: EdgeInsets.all(5),
          child: Container(
            height: 10,
            child: Column(
              children: <Widget>[
                Center(
                  child: MaterialButton(
                      height: 50,
                      color: color,
                      shape: CircleBorder(),
                      onPressed: () {},
                      child: Icon(
                        icon,
                        color: Colors.white,
                      )),
                ),
                Text(
                  menuName,
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 10),
                )
              ],
            ),
          ));
    }

    void _showBottomSheet() {
      showModalBottomSheet(
          context: context,
          isScrollControlled: true,
          builder: (context) {
            return Container(
              height: height * 0.88,
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(25),
                      topRight: Radius.circular(25))),
              child: Column(
                children: <Widget>[
                  // Text('Features'),
                  Expanded(
                      flex: 1,
                      child: Container(
                          padding: EdgeInsets.fromLTRB(25, 15, 25, 15),
                          alignment: Alignment.topLeft,
                          child: Text(
                            'Features',
                            style: TextStyle(
                                fontWeight: FontWeight.bold, fontSize: 20),
                          ))),
                  Expanded(
                    flex: 9,
                    child: Padding(
                      padding: EdgeInsets.fromLTRB(15, 0, 15, 0),
                      child: GridView(
                        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 4,
                          mainAxisSpacing: 4,
                          crossAxisSpacing: 4,
                          childAspectRatio: itemWidth / 180,
                        ),
                        children: <Widget>[
                          _itemMenuAlt(Color.fromRGBO(177, 0, 26, 1), Icons.tv,
                              'Live Show'),
                          _itemMenuAlt(Color.fromRGBO(177, 0, 26, 1),
                              Icons.signal_wifi_4_bar_sharp, 'Live Class'),
                          _itemMenuAlt(Color.fromRGBO(177, 0, 26, 1),
                              Icons.book, 'E-Courses'),
                          _itemMenuAlt(Color.fromRGBO(177, 0, 26, 1),
                              Icons.group, 'Community'),
                          _itemMenuAlt(
                              Colors.blue, Icons.play_arrow, 'Saved Course'),
                          _itemMenuAlt(
                              Colors.blue, Icons.person, 'Saved Course'),
                          _itemMenuAlt(
                              Colors.blue, Icons.disc_full, 'Recent Courses'),
                          _itemMenuAlt(Colors.blue, Icons.list, 'My List'),
                          _itemMenuAlt(
                              Colors.green, Icons.add_chart, 'My Chart'),
                          _itemMenuAlt(
                              Colors.green, Icons.history, 'Purchase History'),
                          _itemMenuAlt(
                              Colors.grey[300],
                              Icons.transfer_within_a_station,
                              'Marketplace (Beta)'),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            );
          });
    }

    Widget _itemMenu(Color color, IconData icon, String menuName) {
      return Expanded(
          child: Column(
        children: <Widget>[
          MaterialButton(
            height: 50,
            color: color,
            shape: CircleBorder(),
            onPressed: () {
              _showBottomSheet();
            },
            child: Icon(
              icon,
              color: Colors.white,
            ),
          ),
          Text(
            menuName,
            style: TextStyle(fontSize: 10),
          )
        ],
      ));
    }

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(177, 0, 26, 1),
        title: Text('List Product'),
        centerTitle: true,
      ),
      body: Stack(
        children: <Widget>[
          Container(
            width: width,
            height: height,
            color: Colors.white,
            child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: FutureBuilder(
                    future: apiService.getListFood(),
                    builder: (BuildContext context, AsyncSnapshot snapshot) {
                      List<Food> food = snapshot.data;
                      if (snapshot.hasError) {
                        return Center(
                          child: Text(
                              "Something wrong with message: ${snapshot.error.toString()}"),
                        );
                      } else if (snapshot.connectionState ==
                          ConnectionState.done) {
                        return GridView.builder(
                            itemCount: snapshot.data.length,
                            gridDelegate:
                                SliverGridDelegateWithFixedCrossAxisCount(
                              crossAxisCount: 2,
                              mainAxisSpacing: 2,
                              crossAxisSpacing: 2,
                              childAspectRatio: itemWidth / 180,
                            ),
                            itemBuilder: (context, index) {
                              Food itemFood = food[index];
                              var itemFoodDetail = itemFood;
                              return _itemFood(itemFoodDetail);
                            });
                      } else {
                        return Center(
                          child: CircularProgressIndicator(
                            backgroundColor: Color.fromRGBO(177, 0, 26, 1),
                            // valueColor: Animation<Color.fromRGBO(177, 0, 26, 1)>,
                          ),
                        );
                      }
                    })),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Padding(
              padding: EdgeInsets.fromLTRB(18, 0, 18, 0),
              child: GestureDetector(
                onTap: () {
                  _showBottomSheet();
                },
                child: Container(
                  width: width,
                  height: height * 0.14,
                  margin: EdgeInsets.only(bottom: 25),
                  child: Column(
                    children: <Widget>[
                      Expanded(
                        flex: 1,
                        child: Container(
                            width: 20,
                            child: GestureDetector(
                              onTap: () {
                                _showBottomSheet();
                              },
                              child: Divider(
                                color: Color.fromRGBO(177, 0, 26, 1),
                                thickness: 2,
                              ),
                            )),
                      ),
                      Expanded(
                        flex: 9,
                        child: Padding(
                          padding: EdgeInsets.fromLTRB(5, 10, 5, 5),
                          child: Row(
                            children: <Widget>[
                              _itemMenu(Color.fromRGBO(177, 0, 26, 1), Icons.tv,
                                  'Live Show'),
                              _itemMenu(Color.fromRGBO(177, 0, 26, 1),
                                  Icons.signal_wifi_4_bar_sharp, 'Live Class'),
                              _itemMenu(Color.fromRGBO(177, 0, 26, 1),
                                  Icons.book, 'E-Courses'),
                              _itemMenu(Color.fromRGBO(177, 0, 26, 1),
                                  Icons.group, 'Community'),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(35),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey.withOpacity(0.5),
                          spreadRadius: 5,
                          blurRadius: 7,
                          offset: Offset(0, 3), // changes position of shadow
                        ),
                      ]),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
